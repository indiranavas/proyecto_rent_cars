package Steps;

import java.awt.Font;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

public class InternalTrip extends javax.swing.JInternalFrame {
    private Connection connection = null;
    private ResultSet rs = null;
    private Statement s = null;
    DefaultTableModel md;
    JTableHeader th;
    Conexiones.msjSistema CargarMsj = new Conexiones.msjSistema();
    DecimalFormat MaskCod= new DecimalFormat("000");
    DecimalFormat MaskDiaMes= new DecimalFormat("00");
    SimpleDateFormat MaskFecha = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat MaskFecha2 = new SimpleDateFormat("yyyy-MM-dd");
    String FechaAct;
    Date FechaAct2;
    int ContFecha = 0;
    public static boolean valCampoVehicles = false, valCampoDrivers = false;
    
    public InternalTrip() {
        initComponents();
        setResizable(false);
        md=(DefaultTableModel) tblBusqSteps.getModel();
        tblBusqSteps.setModel(md);
        TableColumn ColmCod=tblBusqSteps.getColumnModel().getColumn(0);
        ColmCod.setMinWidth(40);
        ColmCod.setMaxWidth(40);
        TableColumn ColmVehicles=tblBusqSteps.getColumnModel().getColumn(1);
        ColmVehicles.setMinWidth(150);
        ColmVehicles.setMaxWidth(300);
        TableColumn ColmDrivers=tblBusqSteps.getColumnModel().getColumn(2);
        ColmDrivers.setMinWidth(150);
        ColmDrivers.setMaxWidth(300);
        TableColumn ColmDates=tblBusqSteps.getColumnModel().getColumn(3);
        ColmDates.setMinWidth(90);
        ColmDates.setMaxWidth(95);
        //Cambiar fuente al head de la tabla
        th = tblBusqSteps.getTableHeader();
        Font fuente = new Font("Tahoma",Font.BOLD,12);
        th.setFont(fuente);
        //Cambiar altura de las filas
        tblBusqSteps.setRowHeight(20);
        Calendar Fecha = new GregorianCalendar();
        int anho = Fecha.get(Calendar.YEAR);
        int mes = Fecha.get(Calendar.MONTH);
        int dia = Fecha.get(Calendar.DAY_OF_MONTH);
        String nuevFechaDia = MaskDiaMes.format(dia);
        String nuevFechaMes = MaskDiaMes.format(mes+1);
        FechaAct = nuevFechaDia + "/" + nuevFechaMes + "/" + anho;
        FechaAct(FechaAct);
        txtVehicle.requestFocus();
        txtId.setText(MaskCod.format(busqCod()));
        busqSteps();
    }
public void abrirBD(){
    Conexiones.conectarBd CargarBD = new Conexiones.conectarBd();
    CargarBD.CargarBD();
    connection= CargarBD.connection;
}
public void cerrarBD(){
    try {
        connection.close();
    } catch (SQLException ex) {
    }
}
public int busqCod(){
    int Id = 0;
    try {
        this.abrirBD();
        s = connection.createStatement();
        rs = s.executeQuery("SELECT MAX(id) FROM trip;");
    } catch (SQLException e) {
        CargarMsj.msjError(this, "When Connecting.");
    }
    try {
        while (rs.next()) {
            Id = rs.getInt(1) + 1;
        }
    } catch (SQLException e) {
        CargarMsj.msjError(this, "When Printing BD.");
    }
    this.cerrarBD();
    return Id;
}
public void busqSteps(){
    int Id;
    String Brand, Model, Name, LastName;
    Date Dates;
    try {
        this.abrirBD();
        s = connection.createStatement();
        rs = s.executeQuery("SELECT t.id, brand, model, name, lastname, dates FROM trip AS t, vehicles AS v, drivers AS d WHERE dates >= '"+FechaAct2+"' AND vehiid = v.id AND drivid = d.id ORDER BY dates, t.id;");
    } catch (SQLException e) {
        CargarMsj.msjError(this, "When Connecting.");
    }
    try {
        while (rs.next()) {
            Id = rs.getInt(1);
            Brand = rs.getString(2);
            Model = rs.getString(3);
            Name = rs.getString(4);
            LastName = rs.getString(5);
            Dates = rs.getDate(6);
            Object datos[]={MaskCod.format(Id), Brand + ", " + Model, Name + ", " + LastName , MaskFecha.format(Dates)};
            md.addRow(datos);
        }
    } catch (SQLException e) {
        CargarMsj.msjError(this, "When Printing BD.");
    }
    this.cerrarBD();
}
public void FechaAct(String leerFechaAct){
    try {
        jdDates.setDate(MaskFecha.parse(leerFechaAct));
        FechaAct2 = MaskFecha.parse(leerFechaAct);
        ContFecha++;
    } catch (ParseException ex) {
    }
} 
public void Limpiar(){
    txtId.setText(MaskCod.format(busqCod()));
    limpiarTabla(tblBusqSteps);
    busqSteps();
    txtVehiId.setText("000");
    txtVehicle.setText("");
    txtDrivId.setText("000");
    txtDriver.setText("");
    FechaAct(FechaAct);
    btnAdd.setEnabled(true);
    txtVehiId.requestFocus();
    ContFecha = 1;
    valCampoVehicles = false;
    valCampoDrivers = false;
}
public static int calcFechas(Date FechaDesde, Date FechaHasta) { // PARA SABER LA DIFERENCIA DE DIAS ENTRE 2 FECHAS Q SE PASAN POR PARAMETROS COMO STRING Y RETORNA INT
    DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
    String fechaInicioString = df.format(FechaDesde);
    try {
        FechaDesde = df.parse(fechaInicioString);
    }
    catch (ParseException ex) {
    }
    String fechaFinalString = df.format(FechaHasta);
    try {
        FechaHasta = df.parse(fechaFinalString);
    }
    catch (ParseException ex) {
    }
    long fechaInicialMs = FechaDesde.getTime();
    long fechaFinalMs = FechaHasta.getTime();
    long diferencia = fechaFinalMs - fechaInicialMs;
    double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
    return ( (int) dias);
}
public void limpiarTabla(JTable tabla){
    try {
        DefaultTableModel modelo=(DefaultTableModel) tabla.getModel();
        int filas=tabla.getRowCount();
        for (int i = 0;filas>i; i++) {
            modelo.removeRow(0);
        }
    } catch (Exception e) {
        CargarMsj.msjWarning(this, "Error Cleaning Table.");
    }
}

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnReturn = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        txtId = new javax.swing.JTextField();
        lblCod1 = new javax.swing.JLabel();
        txtDriver = new javax.swing.JTextField();
        lblDate = new javax.swing.JLabel();
        txtVehicle = new javax.swing.JTextField();
        lblVehicle = new javax.swing.JLabel();
        jdDates = new com.toedter.calendar.JDateChooser();
        lblDriver = new javax.swing.JLabel();
        txtDrivId = new javax.swing.JTextField();
        txtVehiId = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblBusqSteps = new javax.swing.JTable();

        setClosable(true);
        setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        setTitle("Trip");
        setPreferredSize(new java.awt.Dimension(566, 438));
        getContentPane().setLayout(null);

        btnReturn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Return32.png"))); // NOI18N
        btnReturn.setToolTipText("Return");
        btnReturn.setBorder(null);
        btnReturn.setBorderPainted(false);
        btnReturn.setContentAreaFilled(false);
        btnReturn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnReturn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReturnActionPerformed(evt);
            }
        });
        getContentPane().add(btnReturn);
        btnReturn.setBounds(500, 350, 40, 40);

        btnLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Limpiar32.png"))); // NOI18N
        btnLimpiar.setToolTipText("Clean");
        btnLimpiar.setBorder(null);
        btnLimpiar.setBorderPainted(false);
        btnLimpiar.setContentAreaFilled(false);
        btnLimpiar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(380, 350, 40, 40);

        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Add32.png"))); // NOI18N
        btnAdd.setToolTipText("Add");
        btnAdd.setBorder(null);
        btnAdd.setBorderPainted(false);
        btnAdd.setContentAreaFilled(false);
        btnAdd.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        getContentPane().add(btnAdd);
        btnAdd.setBounds(340, 350, 40, 40);

        btnUpdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Update32.png"))); // NOI18N
        btnUpdate.setToolTipText("Update");
        btnUpdate.setBorder(null);
        btnUpdate.setBorderPainted(false);
        btnUpdate.setContentAreaFilled(false);
        btnUpdate.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        getContentPane().add(btnUpdate);
        btnUpdate.setBounds(420, 350, 40, 40);

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/MsjNoDelete32.png"))); // NOI18N
        btnDelete.setToolTipText("Delete");
        btnDelete.setBorder(null);
        btnDelete.setBorderPainted(false);
        btnDelete.setContentAreaFilled(false);
        btnDelete.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        getContentPane().add(btnDelete);
        btnDelete.setBounds(460, 350, 40, 40);

        txtId.setEditable(false);
        txtId.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtId.setText("000");
        getContentPane().add(txtId);
        txtId.setBounds(270, 20, 61, 25);

        lblCod1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblCod1.setText("Id:");
        getContentPane().add(lblCod1);
        lblCod1.setBounds(250, 20, 20, 25);

        txtDriver.setEditable(false);
        txtDriver.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDriverKeyTyped(evt);
            }
        });
        getContentPane().add(txtDriver);
        txtDriver.setBounds(170, 85, 335, 25);

        lblDate.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblDate.setText("Date:");
        getContentPane().add(lblDate);
        lblDate.setBounds(50, 110, 45, 25);

        txtVehicle.setEditable(false);
        txtVehicle.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVehicleKeyTyped(evt);
            }
        });
        getContentPane().add(txtVehicle);
        txtVehicle.setBounds(170, 60, 335, 25);

        lblVehicle.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblVehicle.setText("Vehicle:");
        getContentPane().add(lblVehicle);
        lblVehicle.setBounds(50, 60, 45, 25);

        jdDates.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdDatesPropertyChange(evt);
            }
        });
        getContentPane().add(jdDates);
        jdDates.setBounds(100, 110, 130, 25);

        lblDriver.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblDriver.setText("Driver:");
        getContentPane().add(lblDriver);
        lblDriver.setBounds(50, 85, 45, 25);

        txtDrivId.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtDrivId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtDrivId.setText("000");
        txtDrivId.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtDrivIdFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtDrivIdFocusLost(evt);
            }
        });
        txtDrivId.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtDrivIdMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtDrivIdMousePressed(evt);
            }
        });
        txtDrivId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDrivIdKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDrivIdKeyTyped(evt);
            }
        });
        getContentPane().add(txtDrivId);
        txtDrivId.setBounds(100, 85, 61, 25);

        txtVehiId.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtVehiId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtVehiId.setText("000");
        txtVehiId.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtVehiIdFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtVehiIdFocusLost(evt);
            }
        });
        txtVehiId.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtVehiIdMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtVehiIdMousePressed(evt);
            }
        });
        txtVehiId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtVehiIdKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVehiIdKeyTyped(evt);
            }
        });
        getContentPane().add(txtVehiId);
        txtVehiId.setBounds(100, 60, 61, 25);

        tblBusqSteps.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        tblBusqSteps.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "VEHICLES", "DRIVERS", "DATES"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblBusqSteps.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_NEXT_COLUMN);
        tblBusqSteps.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblBusqSteps.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblBusqStepsMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblBusqSteps);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(0, 150, 560, 180);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtVehicleKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVehicleKeyTyped
        char c= evt.getKeyChar();
        if(Character.isLetter(c)) evt.setKeyChar(Character.toUpperCase(c));
        //        if (c<'A' || c>'Z' && c<'a' || c>'z')evt.consume();
        if(txtVehicle.getText().length()>199) evt.consume();
    }//GEN-LAST:event_txtVehicleKeyTyped

    private void txtDriverKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDriverKeyTyped
        char c= evt.getKeyChar();
        if(Character.isLetter(c)) evt.setKeyChar(Character.toUpperCase(c));
        //        if (c<'A' || c>'Z' && c<'a' || c>'z')evt.consume();
        if(txtDriver.getText().length()>199) evt.consume();
    }//GEN-LAST:event_txtDriverKeyTyped

    private void btnReturnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReturnActionPerformed
        dispose();
    }//GEN-LAST:event_btnReturnActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        Limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        String ErrorTxt="Check Fields: ";
        String nuevFecha;
        int Id = Integer.parseInt(txtId.getText());
        int VehiId = Integer.parseInt(txtVehiId.getText());
        int DrivId = Integer.parseInt(txtDrivId.getText());
        Date FechaSelect = jdDates.getDate();
        int valFechas = -1;
        if((FechaAct2 != null) && (FechaSelect != null)){
            valFechas = calcFechas(FechaAct2, FechaSelect);
        }
        if((valCampoVehicles == false) || (valCampoDrivers == false) || (valFechas < 0)){
            if(valCampoVehicles == false) ErrorTxt=ErrorTxt+"<VEHICLES> ";
            if(valCampoDrivers == false) ErrorTxt=ErrorTxt+"<DRIVERS> ";
            if(valFechas < 0) ErrorTxt=ErrorTxt+"<DATE> ";
            CargarMsj.msjWarning(this, ErrorTxt);
        }
        else{
            try {
                nuevFecha = (MaskFecha2.format(jdDates.getDate()));
                this.abrirBD();
                s = connection.createStatement();
                PreparedStatement prs = connection.prepareStatement("INSERT INTO trip (id, vehiid, drivid, dates) VALUES (?, ?, ?, ?);");
                prs.setInt(1, Id);
                prs.setInt(2, VehiId);
                prs.setInt(3, DrivId);
                prs.setString(4, nuevFecha);
                prs.execute();
                CargarMsj.msjSave(this, "Saved Record.");
                Limpiar();
//                int z = s.executeUpdate("INSERT INTO trip (id, vehiid, drivid, dates) VALUES ("+Id+", "+VehiId+", "+DrivId+", '"+FechaSelect+"' );");
//                if (z == 1) {
//                    CargarMsj.msjSave(this, "Saved Record.");
//                    Limpiar();
//                }
            } catch (SQLException e) {
                CargarMsj.msjError(this, "Problems Saving Record.   "+e);
            }
            this.cerrarBD();
        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        String ErrorTxt="Check Fields: ";
        String nuevFecha;
        int Id = Integer.parseInt(txtId.getText());
        int VehiId = Integer.parseInt(txtVehiId.getText());
        int DrivId = Integer.parseInt(txtDrivId.getText());
        Date FechaSelect = jdDates.getDate();
        int valFechas = -1;
        if((FechaAct2 != null) && (FechaSelect != null)){
            valFechas = calcFechas(FechaAct2, FechaSelect);
        }
        if((valCampoVehicles == false) || (valCampoDrivers == false) || (valFechas < 0)){
            if(valCampoVehicles == false) ErrorTxt=ErrorTxt+"<VEHICLES> ";
            if(valCampoDrivers == false) ErrorTxt=ErrorTxt+"<DRIVERS> ";
            if(valFechas < 0) ErrorTxt=ErrorTxt+"<DATE> ";
            CargarMsj.msjWarning(this, ErrorTxt);
        }
        else{
            try {
                nuevFecha = (MaskFecha2.format(jdDates.getDate()));
                this.abrirBD();
                s = connection.createStatement();
//                PreparedStatement prs = connection.prepareStatement("UPDATE trip SET vehiid = "+VehiId+", drivid = "+DrivId+", dates = '"+FechaSelect+"' WHERE id = "+Id+" ;");
                PreparedStatement prs = connection.prepareStatement("UPDATE trip SET vehiid = "+VehiId+", drivid = "+DrivId+", dates = '"+nuevFecha+"' WHERE id = "+Id+" ;");
                prs.executeUpdate();
                CargarMsj.msjSave(this, "Changes saved.");
                Limpiar();
            } catch (SQLException e) {
            }
            this.cerrarBD();
        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int i=tblBusqSteps.getSelectedRow();
        if(i==-1) CargarMsj.msjInformation(this, "You Must Select a Row.");
        else{
            String Id = tblBusqSteps.getValueAt(i,0).toString();
            if(connection!=null){
                if(Id.equals("")){
                    CargarMsj.msjInformation(this, "You Must Search for a Vehicle.");
                }else{
                    try {
                        this.abrirBD();
                        s = connection.createStatement();
                        rs = s.executeQuery("DELETE FROM trip WHERE id = '"+Id+"' ;");
                    } catch (SQLException e) {
                        CargarMsj.msjSave(this, "Registry Deleted.");
                        Limpiar();
                    }
                    this.cerrarBD();
                }
            }
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void tblBusqStepsMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblBusqStepsMousePressed
        if (evt.getClickCount() == 2) {
            int i=tblBusqSteps.getSelectedRow();
            if(i==-1) CargarMsj.msjInformation(this, "You Must Select a Row.");
            else{
                ContFecha = 0;
                int Id, VehiId, DrivId;
                String Brand, Model, Name, LastName;
                Date Dates;
                String envCod = tblBusqSteps.getValueAt(i,0).toString();
                try {
                    this.abrirBD();
                    s = connection.createStatement();
                    rs = s.executeQuery("SELECT t.id, vehiid, brand, model, drivid, name, lastname, dates FROM trip AS t, vehicles AS v, drivers AS d WHERE t.id = "+envCod+" AND vehiid = v.id AND drivid = d.id ;");
                } catch (SQLException e) {
                    CargarMsj.msjError(this, "When Connecting.");
                }
                try {
                    while (rs.next()) {
                        Id = rs.getInt(1);
                        VehiId = rs.getInt(2);
                        Brand = rs.getString(3);
                        Model = rs.getString(4);
                        DrivId = rs.getInt(5);
                        Name = rs.getString(6);
                        LastName = rs.getString(7);
                        Dates = rs.getDate(8);
                        txtId.setText(MaskCod.format(Id));
                        txtVehiId.setText(MaskCod.format(VehiId));
                        txtVehicle.setText(Brand + ", " + Model);
                        txtDrivId.setText(MaskCod.format(DrivId));
                        txtDriver.setText(Name + ", " + LastName);
                        jdDates.setDate(Dates);
                        valCampoVehicles = true;
                        valCampoDrivers  = true;
                        md.removeRow(i);
                        btnAdd.setEnabled(false);
                        ContFecha++;
                    }
                } catch (SQLException e) {
                    CargarMsj.msjError(this, "When Printing BD.");
                }
                this.cerrarBD();
            }
        }
    }//GEN-LAST:event_tblBusqStepsMousePressed

    private void txtVehiIdFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtVehiIdFocusGained
        String leerVehiId = txtVehiId.getText();
        if(leerVehiId.equals("")) leerVehiId = "0";
        int VehiId = Integer.parseInt(leerVehiId);
        if(VehiId == 0) txtVehiId.setText("");
    }//GEN-LAST:event_txtVehiIdFocusGained

    private void txtVehiIdFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtVehiIdFocusLost
        String leerVehiId = txtVehiId.getText();
        if(leerVehiId.equals("")) leerVehiId = "0";
        int VehiId = Integer.parseInt(leerVehiId);
        if(VehiId == 0) txtVehiId.setText("000");
    }//GEN-LAST:event_txtVehiIdFocusLost

    private void txtVehiIdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVehiIdKeyPressed
        char c=evt.getKeyChar();
        if(c==KeyEvent.VK_ENTER) {
            String ErrorTxt="Check Fields: ";
            int Id;
            String Brand, Model, nuevVehicles;
            valCampoVehicles = false;
            boolean Val = false;
            String leerVehiId = txtVehiId.getText();
            if(leerVehiId.equals("")) leerVehiId = "0";
            int VehiId = Integer.parseInt(leerVehiId);
            String leerDates = MaskFecha.format(jdDates.getDate());
            if(VehiId == 0){
                ErrorTxt=ErrorTxt+"<VEHICLES ID.> ";
                CargarMsj.msjWarning(this, ErrorTxt);
                txtVehicle.setText("Does NOT Exist.");
            }
            else{
                try {
                    this.abrirBD();
                    s = connection.createStatement();
                    rs = s.executeQuery("SELECT id, brand, model FROM vehicles WHERE id = "+VehiId+" ;");
                } catch (SQLException e) {
                    CargarMsj.msjError(this, "When Connecting.");
                }
                try {
                    while (rs.next()) {
                        Id = rs.getInt(1);
                        Brand = rs.getString(2);
                        Model = rs.getString(3);
                        nuevVehicles = Brand + ", " + Model;
                        int i = tblBusqSteps.getRowCount();
                        int Cont = 0;
                        if(i >= 0) {
                            while ((Val == false) && (Cont < tblBusqSteps.getRowCount())){
                                String leerHistVehicles = tblBusqSteps.getValueAt(Cont,1).toString();
                                String leerHistDates = tblBusqSteps.getValueAt(Cont,3).toString();
                                if((nuevVehicles.equals(leerHistVehicles)) && (leerDates.equals(leerHistDates))){
                                    CargarMsj.msjWarning(this, "Vehicle Assigned for this Date.");
                                    Val = true;
                                    break;
                                }
                                Cont++;
                            }
                        }
                        if(Val == false){
                            txtVehiId.setText(MaskCod.format(Id));
                            txtVehicle.setText(nuevVehicles);
                            txtDrivId.requestFocus();
                        }
                        valCampoVehicles = true;
                    }
                } catch (SQLException e) {
                    CargarMsj.msjError(this, "When Printing BD.");
                }
                if(valCampoVehicles == false) {
                    txtVehicle.setText("Does NOT Exist.");
                }
                this.cerrarBD();
            }
        }
        if(evt.getKeyCode()==115) {
            InternalSelectTripVehicles obj= new InternalSelectTripVehicles();
            VntPrincipal.jdeskEscritorio.add(obj);
            obj.show();
            InternalSelectTripVehicles.txtBrand.requestFocus();
        }
    }//GEN-LAST:event_txtVehiIdKeyPressed

    private void txtVehiIdKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVehiIdKeyTyped
        char c= evt.getKeyChar();
        if (c<'0' || c>'9')evt.consume();
        if (txtVehiId.getText().length()>2) evt.consume();
    }//GEN-LAST:event_txtVehiIdKeyTyped

    private void txtVehiIdMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtVehiIdMouseClicked
        String leerVehiId = txtVehiId.getText();
        if(leerVehiId.equals("")) leerVehiId = "0";
        int VehiId = Integer.parseInt(leerVehiId);
        if(VehiId == 0) txtVehiId.setText("");
    }//GEN-LAST:event_txtVehiIdMouseClicked

    private void txtVehiIdMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtVehiIdMousePressed
        if (evt.getClickCount() == 2) {
            txtVehiId.setText("");
            valCampoVehicles = false;
        }
    }//GEN-LAST:event_txtVehiIdMousePressed

    private void txtDrivIdFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDrivIdFocusGained
        String leerDrivId = txtDrivId.getText();
        if(leerDrivId.equals("")) leerDrivId = "0";
        int DrivId = Integer.parseInt(leerDrivId);
        if(DrivId == 0) txtDrivId.setText("");
    }//GEN-LAST:event_txtDrivIdFocusGained

    private void txtDrivIdFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDrivIdFocusLost
        String leerDrivId = txtDrivId.getText();
        if(leerDrivId.equals("")) leerDrivId = "0";
        int DrivId = Integer.parseInt(leerDrivId);
        if(DrivId == 0) txtDrivId.setText("000");
    }//GEN-LAST:event_txtDrivIdFocusLost

    private void txtDrivIdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDrivIdKeyPressed
        char c=evt.getKeyChar();
        if(c==KeyEvent.VK_ENTER) {
            String ErrorTxt="Check Fields: ";
            int Id;
            String Name, LastName, nuevDrivers;
            valCampoDrivers = false;
            boolean Val = false;
            String leerDrivId = txtDrivId.getText();
            if(leerDrivId.equals("")) leerDrivId = "0";
            int DrivId = Integer.parseInt(leerDrivId);
            String leerDates = MaskFecha.format(jdDates.getDate());
            if(DrivId == 0){
                ErrorTxt=ErrorTxt+"<DRIVERS ID.> ";
                CargarMsj.msjWarning(this, ErrorTxt);
                txtDriver.setText("Does NOT Exist.");
            }
            else{
                try {
                    this.abrirBD();
                    s = connection.createStatement();
                    rs = s.executeQuery("SELECT id, name, lastname FROM drivers WHERE id = "+DrivId+" ;");
                } catch (SQLException e) {
                    CargarMsj.msjError(this, "When Connecting.");
                }
                try {
                    while (rs.next()) {
                        Id = rs.getInt(1);
                        Name = rs.getString(2);
                        LastName = rs.getString(3);
                        nuevDrivers = Name + ", " + LastName;
                        int i = tblBusqSteps.getRowCount();
                        int Cont = 0;
                        if(i >= 0) {
                            while ((Val == false) && (Cont < tblBusqSteps.getRowCount())){
                                String leerHistDrivers = tblBusqSteps.getValueAt(Cont,2).toString();
                                String leerHistDates = tblBusqSteps.getValueAt(Cont,3).toString();
                                if((nuevDrivers.equals(leerHistDrivers)) && (leerDates.equals(leerHistDates))){
                                    CargarMsj.msjWarning(this, "Drivers Assigned for this Date.");
                                    Val = true;
                                    break;
                                }
                                Cont++;
                            }
                        }
                        if(Val == false){
                            txtDrivId.setText(MaskCod.format(Id));
                            txtDriver.setText(nuevDrivers);
                            jdDates.requestFocus();
                        }
                        valCampoDrivers = true;
                    }
                } catch (SQLException e) {
                    CargarMsj.msjError(this, "When Printing BD.");
                }
                if(valCampoDrivers == false) {
                    txtDriver.setText("Does NOT Exist.");
                }
                this.cerrarBD();
            }
        }
        if(evt.getKeyCode()==115) {
            InternalSelectTripDrivers obj= new InternalSelectTripDrivers();
            VntPrincipal.jdeskEscritorio.add(obj);
            obj.show();
            InternalSelectTripDrivers.txtName.requestFocus();
        }
    }//GEN-LAST:event_txtDrivIdKeyPressed

    private void txtDrivIdKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDrivIdKeyTyped
        char c= evt.getKeyChar();
        if (c<'0' || c>'9')evt.consume();
        if (txtDrivId.getText().length()>2) evt.consume();
    }//GEN-LAST:event_txtDrivIdKeyTyped

    private void txtDrivIdMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtDrivIdMouseClicked
        String leerDrivId = txtDrivId.getText();
        if(leerDrivId.equals("")) leerDrivId = "0";
        int DrivId = Integer.parseInt(leerDrivId);
        if(DrivId == 0) txtDrivId.setText("");
    }//GEN-LAST:event_txtDrivIdMouseClicked

    private void txtDrivIdMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtDrivIdMousePressed
        if (evt.getClickCount() == 2) {
            txtDrivId.setText("");
            valCampoDrivers = false;
        }
    }//GEN-LAST:event_txtDrivIdMousePressed

    private void jdDatesPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdDatesPropertyChange
        String ErrorTxt="Check Fields: ";
        Date FechaSelect = jdDates.getDate();
        String leerDates = MaskFecha.format(FechaSelect);
        int valFechas = -1;
        if((FechaAct2 != null) && (FechaSelect != null)){
            valFechas = calcFechas(FechaAct2, FechaSelect);
        }
        if(ContFecha > 0){
            if(valFechas < 0) {
                ErrorTxt=ErrorTxt+"<DATE INVALID> ";
                CargarMsj.msjWarning(this, ErrorTxt);
                jdDates.setDate(FechaAct2);
            }
            else{
                int VehiId = Integer.parseInt(txtVehiId.getText());
                String Vehicles = txtVehicle.getText();
                int DrivId = Integer.parseInt(txtDrivId.getText());
                String Drivers = txtDriver.getText();
                boolean Val1 = false, Val2 = false;
                int i = tblBusqSteps.getRowCount();
                int Cont1 = 0, Cont2 = 0;
                if(VehiId > 0){
                    if(i >= 0) {
                        while ((Val1 == false) && (Cont1 < tblBusqSteps.getRowCount())){
                            String leerHistVehicles = tblBusqSteps.getValueAt(Cont1,1).toString();
                            String leerHistDates = tblBusqSteps.getValueAt(Cont1,3).toString();
                            if((Vehicles.equals(leerHistVehicles)) && (leerDates.equals(leerHistDates))){
                                Val1 = true;
                                break;
                            }
                            Cont1++;
                        }
                    }
                    if(Val1 == true){
                        ErrorTxt=ErrorTxt+"<VEHICLES INVALID> ";
                        valCampoVehicles = false;
                    }
                }
                if(DrivId > 0){
                    if(i >= 0) {
                        while ((Val2 == false) && (Cont2 < tblBusqSteps.getRowCount())){
                            String leerHistDrivers = tblBusqSteps.getValueAt(Cont2,2).toString();
                            String leerHistDates = tblBusqSteps.getValueAt(Cont2,3).toString();
                            if((Drivers.equals(leerHistDrivers)) && (leerDates.equals(leerHistDates))){
                                Val2 = true;
                                break;
                            }
                            Cont2++;
                        }
                    }
                    if(Val2 == true){
                        ErrorTxt=ErrorTxt+"<DRIVERS INVALID> ";
                        valCampoDrivers = false;
                    }
                }
                if((Val1 == true) || (Val2 == true)) CargarMsj.msjWarning(this, ErrorTxt);
            }
        }
    }//GEN-LAST:event_jdDatesPropertyChange

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnReturn;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JScrollPane jScrollPane1;
    public static com.toedter.calendar.JDateChooser jdDates;
    private javax.swing.JLabel lblCod1;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblDriver;
    private javax.swing.JLabel lblVehicle;
    public static javax.swing.JTable tblBusqSteps;
    public static javax.swing.JTextField txtDrivId;
    public static javax.swing.JTextField txtDriver;
    public static javax.swing.JTextField txtId;
    public static javax.swing.JTextField txtVehiId;
    public static javax.swing.JTextField txtVehicle;
    // End of variables declaration//GEN-END:variables
}
