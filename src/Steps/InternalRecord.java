package Steps;

import java.awt.Font;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

public class InternalRecord extends javax.swing.JInternalFrame {
    private Connection connection = null;
    private ResultSet rs = null;
    private Statement s = null;
    public static DefaultTableModel md;
    JTableHeader th;
    Conexiones.msjSistema CargarMsj = new Conexiones.msjSistema();
    DecimalFormat MaskCod= new DecimalFormat("000");
    DecimalFormat MaskDiaMes= new DecimalFormat("00");
    SimpleDateFormat MaskFecha = new SimpleDateFormat("dd/MM/yyyy");
    String FechaAct;
    Date FechaAct2;
    int ContFecha = 0;
    public static boolean valCampoVehicles = false, valCampoDrivers = false;
    boolean valFHasta = false;
    
    public InternalRecord() {
        initComponents();
        setResizable(false);
        md=(DefaultTableModel) tblBusqSteps.getModel();
        tblBusqSteps.setModel(md);
        TableColumn ColmCod=tblBusqSteps.getColumnModel().getColumn(0);
        ColmCod.setMinWidth(40);
        ColmCod.setMaxWidth(40);
        TableColumn ColmDates=tblBusqSteps.getColumnModel().getColumn(1);
        ColmDates.setMinWidth(90);
        ColmDates.setMaxWidth(95);
        TableColumn ColmVehicles=tblBusqSteps.getColumnModel().getColumn(2);
        ColmVehicles.setMinWidth(150);
        ColmVehicles.setMaxWidth(300);
        TableColumn ColmDrivers=tblBusqSteps.getColumnModel().getColumn(3);
        ColmDrivers.setMinWidth(150);
        ColmDrivers.setMaxWidth(300);
        
        //Cambiar fuente al head de la tabla
        th = tblBusqSteps.getTableHeader();
        Font fuente = new Font("Tahoma",Font.BOLD,12);
        th.setFont(fuente);
        //Cambiar altura de las filas
        tblBusqSteps.setRowHeight(20);
        Calendar Fecha = new GregorianCalendar();
        int anho = Fecha.get(Calendar.YEAR);
        int mes = Fecha.get(Calendar.MONTH);
        int dia = Fecha.get(Calendar.DAY_OF_MONTH);
        String nuevFechaDia = MaskDiaMes.format(dia);
        String nuevFechaMes = MaskDiaMes.format(mes+1);
        FechaAct = nuevFechaDia + "/" + nuevFechaMes + "/" + anho;
        FechaAct(FechaAct);
        txtVehiId.requestFocus();
        busqRecord("Z");
        valFHasta = true;
    }
public void abrirBD(){
    Conexiones.conectarBd CargarBD = new Conexiones.conectarBd();
    CargarBD.CargarBD();
    connection= CargarBD.connection;
}
public void cerrarBD(){
    try {
        connection.close();
    } catch (SQLException ex) {
    }
}
public void busqRecord(String Busq){
    int Id;
    String Brand, Model, Name, LastName;
    Date Dates;
    int VehiId = Integer.parseInt(txtVehiId.getText());
    int DrivId = Integer.parseInt(txtDrivId.getText());
    int valFechas = -1;
    Date FechaDesde = jfDesde.getDate();
    Date FechaHasta = jfHasta.getDate();
    try {
        this.abrirBD();
        s = connection.createStatement();
        switch(Busq) {
            case "V": 
                rs = s.executeQuery("SELECT t.id, brand, model, name, lastname, dates FROM trip AS t, vehicles AS v, drivers AS d WHERE vehiid = "+VehiId+" AND vehiid = v.id AND drivid = d.id ORDER BY dates, t.id;");
                limpiarTabla(tblBusqSteps);
                break;
            case "D": 
                if(VehiId == 0) rs = s.executeQuery("SELECT t.id, brand, model, name, lastname, dates FROM trip AS t, vehicles AS v, drivers AS d WHERE drivid = "+DrivId+" AND vehiid = v.id AND drivid = d.id ORDER BY dates, t.id;");
                else rs = s.executeQuery("SELECT t.id, brand, model, name, lastname, dates FROM trip AS t, vehicles AS v, drivers AS d WHERE drivid = "+DrivId+" AND vehiid = "+VehiId+" AND vehiid = v.id AND drivid = d.id ORDER BY dates, t.id;");
                limpiarTabla(tblBusqSteps);
                break;
            case "F": 
                if((FechaDesde != null) && (FechaHasta != null)) valFechas = calcFechas(FechaDesde, FechaHasta);
                if(valFechas >= 0) {
                    if(DrivId == 0){
                        if(VehiId == 0) rs = s.executeQuery("SELECT t.id, brand, model, name, lastname, dates FROM trip AS t, vehicles AS v, drivers AS d WHERE dates BETWEEN '"+FechaDesde+"' AND '"+FechaHasta+"' AND vehiid = v.id AND drivid = d.id ORDER BY dates, t.id;");
                        else rs = s.executeQuery("SELECT t.id, brand, model, name, lastname, dates FROM trip AS t, vehicles AS v, drivers AS d WHERE dates BETWEEN '"+FechaDesde+"' AND '"+FechaHasta+"' AND vehiid = "+VehiId+" AND vehiid = v.id AND drivid = d.id ORDER BY dates, t.id;");
                    }
                    else{
                        if(VehiId == 0) rs = s.executeQuery("SELECT t.id, brand, model, name, lastname, dates FROM trip AS t, vehicles AS v, drivers AS d WHERE dates BETWEEN '"+FechaDesde+"' AND '"+FechaHasta+"' AND drivid = "+DrivId+" AND vehiid = v.id AND drivid = d.id ORDER BY dates, t.id;");
                        else rs = s.executeQuery("SELECT t.id, brand, model, name, lastname, dates FROM trip AS t, vehicles AS v, drivers AS d WHERE dates BETWEEN '"+FechaDesde+"' AND '"+FechaHasta+"' AND drivid = "+DrivId+" AND vehiid = "+VehiId+" AND vehiid = v.id AND drivid = d.id ORDER BY dates, t.id;");
                    }
                    limpiarTabla(tblBusqSteps);
                }
                break;
            default: 
                rs = s.executeQuery("SELECT t.id, brand, model, name, lastname, dates FROM trip AS t, vehicles AS v, drivers AS d WHERE vehiid = v.id AND drivid = d.id ORDER BY dates, t.id;");
                limpiarTabla(tblBusqSteps);
                break;
        }
    } catch (SQLException e) {
        CargarMsj.msjError(this, "When Connecting.");
    }
    try {
        while (rs.next()) {
            Id = rs.getInt(1);
            Brand = rs.getString(2);
            Model = rs.getString(3);
            Name = rs.getString(4);
            LastName = rs.getString(5);
            Dates = rs.getDate(6);
            Object datos[]={MaskCod.format(Id), MaskFecha.format(Dates), Brand + ", " + Model, Name + ", " + LastName };
            md.addRow(datos);
        }
    } catch (SQLException e) {
        CargarMsj.msjError(this, "When Printing BD.");
    }
    this.cerrarBD();
}
public void FechaAct(String leerFechaAct){
    try {
        FechaAct2 = MaskFecha.parse(leerFechaAct);
        ContFecha++;
    } catch (ParseException ex) {
    }
} 
public void Limpiar(){
    txtVehiId.setText("000");
    txtVehicle.setText("");
    txtDrivId.setText("000");
    txtDriver.setText("");
    jfDesde.setDate(null);
    jfHasta.setDate(null);
    busqRecord("Z");
    txtVehiId.requestFocus();
}
public static int calcFechas(Date FechaDesde, Date FechaHasta) { // PARA SABER LA DIFERENCIA DE DIAS ENTRE 2 FECHAS Q SE PASAN POR PARAMETROS COMO STRING Y RETORNA INT
    DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
    String fechaInicioString = df.format(FechaDesde);
    try {
        FechaDesde = df.parse(fechaInicioString);
    }
    catch (ParseException ex) {
    }
    String fechaFinalString = df.format(FechaHasta);
    try {
        FechaHasta = df.parse(fechaFinalString);
    }
    catch (ParseException ex) {
    }
    long fechaInicialMs = FechaDesde.getTime();
    long fechaFinalMs = FechaHasta.getTime();
    long diferencia = fechaFinalMs - fechaInicialMs;
    double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
    return ( (int) dias);
}
public void limpiarTabla(JTable tabla){
    try {
        DefaultTableModel modelo=(DefaultTableModel) tabla.getModel();
        int filas=tabla.getRowCount();
        for (int i = 0;filas>i; i++) {
            modelo.removeRow(0);
        }
    } catch (Exception e) {
        CargarMsj.msjWarning(this, "Error Cleaning Table.");
    }
}
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnReturn = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        pnlDRegistro = new javax.swing.JPanel();
        lblVehicle = new javax.swing.JLabel();
        lblDesde = new javax.swing.JLabel();
        txtDrivId = new javax.swing.JTextField();
        txtVehiId = new javax.swing.JTextField();
        txtVehicle = new javax.swing.JTextField();
        txtDriver = new javax.swing.JTextField();
        jfDesde = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();
        lblCod3 = new javax.swing.JLabel();
        jfHasta = new com.toedter.calendar.JDateChooser();
        lblDriver1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblBusqSteps = new javax.swing.JTable();

        setClosable(true);
        setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        setTitle("Record");
        setPreferredSize(new java.awt.Dimension(650, 438));
        getContentPane().setLayout(null);

        btnReturn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Return32.png"))); // NOI18N
        btnReturn.setToolTipText("Return");
        btnReturn.setBorder(null);
        btnReturn.setBorderPainted(false);
        btnReturn.setContentAreaFilled(false);
        btnReturn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnReturn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReturnActionPerformed(evt);
            }
        });
        getContentPane().add(btnReturn);
        btnReturn.setBounds(570, 350, 40, 40);

        btnLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Limpiar32.png"))); // NOI18N
        btnLimpiar.setToolTipText("Clean");
        btnLimpiar.setBorder(null);
        btnLimpiar.setBorderPainted(false);
        btnLimpiar.setContentAreaFilled(false);
        btnLimpiar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(530, 350, 40, 40);

        pnlDRegistro.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        pnlDRegistro.setOpaque(false);
        pnlDRegistro.setLayout(null);

        lblVehicle.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblVehicle.setText("Vehicle:");
        pnlDRegistro.add(lblVehicle);
        lblVehicle.setBounds(20, 20, 45, 25);

        lblDesde.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblDesde.setText("Desde:");
        pnlDRegistro.add(lblDesde);
        lblDesde.setBounds(20, 70, 45, 25);

        txtDrivId.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtDrivId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtDrivId.setText("000");
        txtDrivId.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtDrivIdFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtDrivIdFocusLost(evt);
            }
        });
        txtDrivId.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtDrivIdMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtDrivIdMousePressed(evt);
            }
        });
        txtDrivId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDrivIdKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDrivIdKeyTyped(evt);
            }
        });
        pnlDRegistro.add(txtDrivId);
        txtDrivId.setBounds(70, 45, 61, 25);

        txtVehiId.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtVehiId.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtVehiId.setText("000");
        txtVehiId.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtVehiIdFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtVehiIdFocusLost(evt);
            }
        });
        txtVehiId.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtVehiIdMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtVehiIdMousePressed(evt);
            }
        });
        txtVehiId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtVehiIdKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVehiIdKeyTyped(evt);
            }
        });
        pnlDRegistro.add(txtVehiId);
        txtVehiId.setBounds(70, 20, 61, 25);

        txtVehicle.setEditable(false);
        txtVehicle.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVehicleKeyTyped(evt);
            }
        });
        pnlDRegistro.add(txtVehicle);
        txtVehicle.setBounds(140, 20, 335, 25);

        txtDriver.setEditable(false);
        txtDriver.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDriverKeyTyped(evt);
            }
        });
        pnlDRegistro.add(txtDriver);
        txtDriver.setBounds(140, 45, 335, 25);

        jfDesde.setOpaque(false);
        jfDesde.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jfDesdePropertyChange(evt);
            }
        });
        pnlDRegistro.add(jfDesde);
        jfDesde.setBounds(70, 70, 130, 25);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("/");
        pnlDRegistro.add(jLabel1);
        jLabel1.setBounds(210, 70, 7, 25);

        lblCod3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblCod3.setText("Hasta:");
        pnlDRegistro.add(lblCod3);
        lblCod3.setBounds(230, 70, 36, 25);

        jfHasta.setOpaque(false);
        jfHasta.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jfHastaPropertyChange(evt);
            }
        });
        pnlDRegistro.add(jfHasta);
        jfHasta.setBounds(280, 70, 130, 25);

        lblDriver1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblDriver1.setText("Driver:");
        pnlDRegistro.add(lblDriver1);
        lblDriver1.setBounds(20, 45, 45, 25);

        getContentPane().add(pnlDRegistro);
        pnlDRegistro.setBounds(60, 20, 500, 115);

        tblBusqSteps.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        tblBusqSteps.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "FECHA", "VEHICLES", "DRIVERS"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblBusqSteps.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_NEXT_COLUMN);
        tblBusqSteps.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jScrollPane1.setViewportView(tblBusqSteps);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(0, 140, 644, 200);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnReturnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReturnActionPerformed
        dispose();
    }//GEN-LAST:event_btnReturnActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        Limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void txtDrivIdFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDrivIdFocusGained
        String leerDrivId = txtDrivId.getText();
        if(leerDrivId.equals("")) leerDrivId = "0";
        int DrivId = Integer.parseInt(leerDrivId);
        if(DrivId == 0) txtDrivId.setText("");
    }//GEN-LAST:event_txtDrivIdFocusGained

    private void txtDrivIdFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDrivIdFocusLost
        String leerDrivId = txtDrivId.getText();
        if(leerDrivId.equals("")) leerDrivId = "0";
        int DrivId = Integer.parseInt(leerDrivId);
        if(DrivId == 0) txtDrivId.setText("000");
    }//GEN-LAST:event_txtDrivIdFocusLost

    private void txtDrivIdMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtDrivIdMouseClicked
        String leerDrivId = txtDrivId.getText();
        if(leerDrivId.equals("")) leerDrivId = "0";
        int DrivId = Integer.parseInt(leerDrivId);
        if(DrivId == 0) txtDrivId.setText("");
    }//GEN-LAST:event_txtDrivIdMouseClicked

    private void txtDrivIdMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtDrivIdMousePressed
        if (evt.getClickCount() == 2) {
            txtDrivId.setText("");
            valCampoDrivers = false;
        }
    }//GEN-LAST:event_txtDrivIdMousePressed

    private void txtDrivIdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDrivIdKeyPressed
        char c=evt.getKeyChar();
        if(c==KeyEvent.VK_ENTER) {
            String ErrorTxt="Check Fields: ";
            int Id;
            String Name, LastName, nuevDrivers;
            valCampoDrivers = false;
            String leerDrivId = txtDrivId.getText();
            if(leerDrivId.equals("")) leerDrivId = "0";
            int DrivId = Integer.parseInt(leerDrivId);
            if(DrivId == 0){
                ErrorTxt=ErrorTxt+"<DRIVERS ID.> ";
                CargarMsj.msjWarning(this, ErrorTxt);
                txtDriver.setText("Does NOT Exist.");
            }
            else{
                try {
                    this.abrirBD();
                    s = connection.createStatement();
                    rs = s.executeQuery("SELECT id, name, lastname FROM drivers WHERE id = "+DrivId+" ;");
                } catch (SQLException e) {
                    CargarMsj.msjError(this, "When Connecting.");
                }
                try {
                    while (rs.next()) {
                        Id = rs.getInt(1);
                        Name = rs.getString(2);
                        LastName = rs.getString(3);
                        nuevDrivers = Name + ", " + LastName;
                        txtDrivId.setText(MaskCod.format(Id));
                        txtDriver.setText(nuevDrivers);
                        busqRecord("D");
                        jfDesde.setDate(null);
                        jfHasta.setDate(null);
                        jfDesde.requestFocusInWindow();
                        valCampoDrivers = true;
                    }
                } catch (SQLException e) {
                    CargarMsj.msjError(this, "When Printing BD.");
                }
                if(valCampoDrivers == false) {
                    txtDriver.setText("Does NOT Exist.");
                }
                this.cerrarBD();
            }
        }
        if(evt.getKeyCode()==115) {
            InternalSelectRecordDrivers obj= new InternalSelectRecordDrivers();
            VntPrincipal.jdeskEscritorio.add(obj);
            obj.show();
            InternalSelectRecordDrivers.txtName.requestFocus();
        }
    }//GEN-LAST:event_txtDrivIdKeyPressed

    private void txtDrivIdKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDrivIdKeyTyped
        char c= evt.getKeyChar();
        if (c<'0' || c>'9')evt.consume();
        if (txtDrivId.getText().length()>2) evt.consume();
    }//GEN-LAST:event_txtDrivIdKeyTyped

    private void txtVehiIdFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtVehiIdFocusGained
        String leerVehiId = txtVehiId.getText();
        if(leerVehiId.equals("")) leerVehiId = "0";
        int VehiId = Integer.parseInt(leerVehiId);
        if(VehiId == 0) txtVehiId.setText("");
    }//GEN-LAST:event_txtVehiIdFocusGained

    private void txtVehiIdFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtVehiIdFocusLost
        String leerVehiId = txtVehiId.getText();
        if(leerVehiId.equals("")) leerVehiId = "0";
        int VehiId = Integer.parseInt(leerVehiId);
        if(VehiId == 0) txtVehiId.setText("000");
    }//GEN-LAST:event_txtVehiIdFocusLost

    private void txtVehiIdMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtVehiIdMouseClicked
        String leerVehiId = txtVehiId.getText();
        if(leerVehiId.equals("")) leerVehiId = "0";
        int VehiId = Integer.parseInt(leerVehiId);
        if(VehiId == 0) txtVehiId.setText("");
    }//GEN-LAST:event_txtVehiIdMouseClicked

    private void txtVehiIdMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtVehiIdMousePressed
        if (evt.getClickCount() == 2) {
            txtVehiId.setText("");
            valCampoVehicles = false;
        }
    }//GEN-LAST:event_txtVehiIdMousePressed

    private void txtVehiIdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVehiIdKeyPressed
        char c=evt.getKeyChar();
        if(c==KeyEvent.VK_ENTER) {
            String ErrorTxt="Check Fields: ";
            int Id;
            String Brand, Model, nuevVehicles;
            valCampoVehicles = false;
            boolean Val = false;
            String leerVehiId = txtVehiId.getText();
            if(leerVehiId.equals("")) leerVehiId = "0";
            int VehiId = Integer.parseInt(leerVehiId);
            if(VehiId == 0){
                ErrorTxt=ErrorTxt+"<VEHICLES ID.> ";
                CargarMsj.msjWarning(this, ErrorTxt);
                txtVehicle.setText("Does NOT Exist.");
            }
            else{
                try {
                    this.abrirBD();
                    s = connection.createStatement();
                    rs = s.executeQuery("SELECT id, brand, model FROM vehicles WHERE id = "+VehiId+" ;");
                } catch (SQLException e) {
                    CargarMsj.msjError(this, "When Connecting.");
                }
                try {
                    while (rs.next()) {
                        Id = rs.getInt(1);
                        Brand = rs.getString(2);
                        Model = rs.getString(3);
                        nuevVehicles = Brand + ", " + Model;
                        txtVehiId.setText(MaskCod.format(Id));
                        txtVehicle.setText(nuevVehicles);
                        busqRecord("V");
                        txtDrivId.setText("000");
                        txtDriver.setText("");
                        jfDesde.setDate(null);
                        jfHasta.setDate(null);
                        txtDrivId.requestFocus();
                        valCampoVehicles = true;
                    }
                } catch (SQLException e) {
                    CargarMsj.msjError(this, "When Printing BD.");
                }
                if(valCampoVehicles == false) {
                    txtVehicle.setText("Does NOT Exist.");
                }
                this.cerrarBD();
            }
        }
        if(evt.getKeyCode()==115) {
            InternalSelectRecordVehicles obj= new InternalSelectRecordVehicles();
            VntPrincipal.jdeskEscritorio.add(obj);
            obj.show();
            InternalSelectRecordVehicles.txtBrand.requestFocus();
        }
    }//GEN-LAST:event_txtVehiIdKeyPressed

    private void txtVehiIdKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVehiIdKeyTyped
        char c= evt.getKeyChar();
        if (c<'0' || c>'9')evt.consume();
        if (txtVehiId.getText().length()>2) evt.consume();
    }//GEN-LAST:event_txtVehiIdKeyTyped

    private void txtVehicleKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVehicleKeyTyped
        char c= evt.getKeyChar();
        if(Character.isLetter(c)) evt.setKeyChar(Character.toUpperCase(c));
        //        if (c<'A' || c>'Z' && c<'a' || c>'z')evt.consume();
        if(txtVehicle.getText().length()>199) evt.consume();
    }//GEN-LAST:event_txtVehicleKeyTyped

    private void txtDriverKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDriverKeyTyped
        char c= evt.getKeyChar();
        if(Character.isLetter(c)) evt.setKeyChar(Character.toUpperCase(c));
        //        if (c<'A' || c>'Z' && c<'a' || c>'z')evt.consume();
        if(txtDriver.getText().length()>199) evt.consume();
    }//GEN-LAST:event_txtDriverKeyTyped

    private void jfDesdePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jfDesdePropertyChange
        if(valFHasta == true){
            Date FechaDesde = jfDesde.getDate();
            Date FechaHasta = jfHasta.getDate();
            int valFechas = -1;
            if((FechaDesde != null) && (FechaHasta != null)) valFechas = calcFechas(FechaDesde, FechaHasta);
            if(valFechas >= 0) busqRecord("F");
        }
    }//GEN-LAST:event_jfDesdePropertyChange

    private void jfHastaPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jfHastaPropertyChange
        if(valFHasta == true){
            busqRecord("F");
        }
    }//GEN-LAST:event_jfHastaPropertyChange

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnReturn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    public static com.toedter.calendar.JDateChooser jfDesde;
    public static com.toedter.calendar.JDateChooser jfHasta;
    private javax.swing.JLabel lblCod3;
    private javax.swing.JLabel lblDesde;
    private javax.swing.JLabel lblDriver1;
    private javax.swing.JLabel lblVehicle;
    private javax.swing.JPanel pnlDRegistro;
    public static javax.swing.JTable tblBusqSteps;
    public static javax.swing.JTextField txtDrivId;
    public static javax.swing.JTextField txtDriver;
    public static javax.swing.JTextField txtVehiId;
    public static javax.swing.JTextField txtVehicle;
    // End of variables declaration//GEN-END:variables
}
