package Conexiones;

import java.awt.Component;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author MSC IN SYSTEMS
 */
public class msjSistema {
    public void msjInformation(Component obj, String Msj){                             //Guardar Registros
        String path = "/icon/MsjInformation48.png";
        URL url = this.getClass().getResource(path);
        ImageIcon icon = new ImageIcon(url);
        JOptionPane.showMessageDialog(null, Msj,"Información.",JOptionPane.PLAIN_MESSAGE,icon);
    }
    public void msjSave(Component obj, String Msj){                             //Guardar Registros
        String path = "/icon/MsjSave48.png";
        URL url = this.getClass().getResource(path);
        ImageIcon icon = new ImageIcon(url);
        JOptionPane.showMessageDialog(null, Msj,"Guardando.",JOptionPane.PLAIN_MESSAGE,icon);
    }
    public void msjWarning(Component obj, String Msj){                          //Verificacion de Datos al Guardar
        String path = "/icon/MsjWarning48.png";
        URL url = this.getClass().getResource(path);
        ImageIcon icon = new ImageIcon(url);
        JOptionPane.showMessageDialog( obj, Msj,"Peligro.",JOptionPane.PLAIN_MESSAGE,icon);
    }
    public void msjError(Component obj, String Msj){                            //Guardar, Buscar, Imprimir B.D.
        String path = "/icon/MsjError48.png";
        URL url = this.getClass().getResource(path);
        ImageIcon icon = new ImageIcon(url);
        JOptionPane.showMessageDialog( obj, Msj,"Error.",JOptionPane.PLAIN_MESSAGE,icon);
    }
    public int msjQuestion(Component obj, String Msj, String[] options){              //Preguntas: "S" para Guardar y "P" para imprimir              
        String path = "/icon/MsjQuestion48.png";
        URL url = this.getClass().getResource(path);
        ImageIcon icon = new ImageIcon(url);
        return JOptionPane.showOptionDialog(null, Msj, "Alerta!", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, icon, options, options[0]);
//        if(Tip.equals("S")) {
////           String[] options = {"Si", "No", "Cancel"};
//           return JOptionPane.showOptionDialog(null, Msj, "Alerta!", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, icon, options, options[0]);
//        }
//        else{
////            String[] options = {"Todo", "Selección", "Cancel"};
//            return JOptionPane.showOptionDialog(null, Msj, "Alerta!", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, icon, options, options[0]);
//        }
    }
    public void msjAccess(Component obj, String Msj, String Asuntoj){           //Guardar, Buscar, Imprimir B.D.
        String path = "/icon/MsjAccess48.png";
        URL url = this.getClass().getResource(path);
        ImageIcon icon = new ImageIcon(url);
        JOptionPane.showMessageDialog( obj, Msj, Asuntoj,JOptionPane.PLAIN_MESSAGE,icon);
    }
    public int msjInput(Component obj, String Msj, String Asuntoj){                             //
        return Integer.parseInt(JOptionPane.showInputDialog(obj, Msj, Asuntoj, JOptionPane.PLAIN_MESSAGE));
    }
    public double msjInput2(Component obj, String Msj, String Asuntoj, String[] options){                             //
        String path = "/icon/MsjAccess48.png";
        URL url = this.getClass().getResource(path);
        ImageIcon icon = new ImageIcon(url);
        return Double.parseDouble((String) JOptionPane.showInputDialog(obj, Msj, Asuntoj, JOptionPane.QUESTION_MESSAGE, icon, options, options[0]));
    }
}
