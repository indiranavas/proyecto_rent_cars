
package Conexiones;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.border.Border;

public class VntPrincipalFondo implements Border{
    public BufferedImage back;
 
    public VntPrincipalFondo(String Tip){
        try {
            URL imagePath = null ;
            switch(Tip){
                case "VI1.1":
                    imagePath = new URL(getClass().getResource("/Imagenes/SisVILogo1.1.jpg").toString());   //Ventana Ingreso 1-3
                    break;
                case "VI1.2":
                    imagePath = new URL(getClass().getResource("/Imagenes/SisVILogo1.2.jpg").toString());   //Ventana Ingreso 2-3
                    break;
                case "VI1.3":
                    imagePath = new URL(getClass().getResource("/Imagenes/SisVILogo1.3.jpg").toString());   //Ventana Ingreso 3-3
                    break;
                case "VI2":
                    imagePath = new URL(getClass().getResource("/Imagenes/SisVILogo2.jpg").toString());     //Ventana Ingreso Logo Error
                    break;
                case "VP":
                    imagePath = new URL(getClass().getResource("/Imagenes/SisVPLogo.png").toString());      //Ventana Principal
                    break;
                case "VF":
                    imagePath = new URL(getClass().getResource("/Imagenes/SisVFLogo.jpg").toString());      //Ventana Fondo
                    break;
                default:
//                    imagePath = new URL(getClass().getResource("/Imagenes/Prueba7.1.jpg").toString());
                    break;
            }
            back = ImageIO.read(imagePath);
        } catch (Exception ex) {            
        }
    }

    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        g.drawImage(back, (x + (width - back.getWidth())/2),(y + (height - back.getHeight())/2), null);
    }
 
    public Insets getBorderInsets(Component c) {
        return new Insets(0,0,0,0);
    }
 
    public boolean isBorderOpaque() {
        return false;
    }
 
}
